package aplikacija.za.upravljanje.restoranom.Kontroler;


import aplikacija.za.upravljanje.restoranom.Entiteti.Artikal;
import aplikacija.za.upravljanje.restoranom.Entiteti.NarudzbaDto;
import aplikacija.za.upravljanje.restoranom.Service.NaručeniArtikliService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200/", "http://localhost:8080/"})

@RestController
public class NarudžbaController {

   private final NaručeniArtikliService service;

   public NarudžbaController(NaručeniArtikliService service) {

      this.service = service;
   }

   @PostMapping(value = "/korisnici/{username}/narudzba")
   public ResponseEntity<Artikal> promijeniArtikal(@PathVariable String username, @RequestBody List<NarudzbaDto> naruceniArtikli) {

      service.kreirajNarudzbu(username, naruceniArtikli);
      return new ResponseEntity<>(HttpStatus.OK);
   }

}
