package aplikacija.za.upravljanje.restoranom.Kontroler;

import aplikacija.za.upravljanje.restoranom.Entiteti.Artikal;
import aplikacija.za.upravljanje.restoranom.Service.ArtikalService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200/","http://localhost:8080/"})

@RestController
public class ArtikalController {

   private final ArtikalService service;


   public ArtikalController(ArtikalService service) {

      this.service = service;
   }

   @GetMapping("/korisnici/{username}/artikli")
   public List<Artikal> dohvatiSveArtikle(@PathVariable String username){
         return service.dohvatiSveArtikle(username);
   }
   @GetMapping("/korisnici/{username}/artikli/")
   public List<Artikal> filtrirajSveArtikle(@PathVariable String username, @RequestParam(value = "filtar") String filtar){

      return service.filtrirajSveArtikle(filtar);
   }

   @GetMapping("/korisnici/{username}/artikli/{id}")
   public Artikal dohvatiArtikalPoId(@PathVariable String username, @PathVariable int id){
      return service.dohvatiArtikalPoId(id);
   }

   @PutMapping(value="/korisnici/{username}/artikli/{id}")
   public ResponseEntity<Artikal> promijeniArtikal(@PathVariable String username, @PathVariable int id, @RequestBody Artikal artikal){
      Artikal todoUpdated = service.spremiArtikal(artikal);
      return new ResponseEntity<>(todoUpdated, HttpStatus.OK);
   }
   @PostMapping (value="/korisnici/{username}/artikli")
   public ResponseEntity<Void> promijeniArtikal(@PathVariable String username, @RequestBody Artikal artikal){

      Artikal createdTodo = service.spremiArtikal(artikal);
      URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createdTodo.getId()).toUri();
      return  ResponseEntity.created(uri).build();
   }
   @DeleteMapping("/korisnici/{username}/artikli/{id}")
   public ResponseEntity<Void> obrisiArtikal(@PathVariable String username, @PathVariable int id){

       service.obrisiArtikalPoId(id);
      return ResponseEntity.noContent().build();
   }

   @GetMapping("/")
   public String home() {
      return ("<h1>Welcome</h1>");
   }

   @GetMapping("/admin")
   public String admin() {
      return ("<h1>Welcome Admin</h1>");
   }
}
