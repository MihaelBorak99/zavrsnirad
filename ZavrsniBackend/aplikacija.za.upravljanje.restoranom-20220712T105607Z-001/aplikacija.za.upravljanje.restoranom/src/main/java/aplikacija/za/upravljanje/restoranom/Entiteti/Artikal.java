package aplikacija.za.upravljanje.restoranom.Entiteti;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

//Razmisli dali bi dodaval sastojke unutar svakog itema- think about it s petekom
@Table(name = "menu_item")
@Entity
public class Artikal {

   @Id
   @GeneratedValue
   private int id;
   private double cijena;
   private String kategorija;
   private String naziv;

   public Artikal() {

   }

   public Artikal(int id, double cijena, String kategorija, String naziv) {

      this.id = id;
      this.cijena = cijena;
      this.kategorija = kategorija;
      this.naziv = naziv;
   }

   public int getId() {

      return id;
   }

   public void setId(int id) {

      this.id = id;
   }

   public double getCijena() {

      return cijena;
   }

   public void setCijena(double cijena) {

      this.cijena = cijena;
   }

   public String getKategorija() {

      return kategorija;
   }

   public void setKategorija(String kategorija) {

      this.kategorija = kategorija;
   }

   public String getNaziv() {

      return naziv;
   }

   public void setNaziv(String naziv) {

      this.naziv = naziv;
   }
}

