package aplikacija.za.upravljanje.restoranom.Entiteti;

public class NarudzbaDto {

   private int id;
   private String naziv;
   private int kolicina;
   private double cijena;
   private String kategorija;

   public NarudzbaDto(int id, double cijena, String kategorija, String naziv, int kolicina) {

      this.id = id;
      this.cijena = cijena;
      this.kategorija = kategorija;
      this.naziv = naziv;
      this.kolicina = kolicina;
   }

   public NarudzbaDto() {

   }

   public int getId() {

      return id;
   }

   public void setId(int id) {

      this.id = id;
   }

   public String getNaziv() {

      return naziv;
   }

   public void setNaziv(String naziv) {

      this.naziv = naziv;
   }

   public int getKolicina() {

      return kolicina;
   }

   public void setKolicina(int kolicina) {

      this.kolicina = kolicina;
   }

   public double getCijena() {

      return cijena;
   }

   public void setCijena(double cijena) {

      this.cijena = cijena;
   }

   public String getKategorija() {

      return kategorija;
   }

   public void setKategorija(String kategorija) {

      this.kategorija = kategorija;
   }
}
