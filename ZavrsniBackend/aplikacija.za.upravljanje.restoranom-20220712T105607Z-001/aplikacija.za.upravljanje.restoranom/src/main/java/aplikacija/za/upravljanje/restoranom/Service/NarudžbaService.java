package aplikacija.za.upravljanje.restoranom.Service;

import aplikacija.za.upravljanje.restoranom.Entiteti.Narudžba;
import aplikacija.za.upravljanje.restoranom.Repository.Narudžba_Repository;
import aplikacija.za.upravljanje.restoranom.Entiteti.User;
import aplikacija.za.upravljanje.restoranom.Repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class NarudžbaService {

   private final UserRepository userRepository;
   private final Narudžba_Repository repository;


   public NarudžbaService(UserRepository userRepository, Narudžba_Repository repository) {

      this.userRepository = userRepository;
      this.repository = repository;

   }

   public Narudžba dohvatiNarudžbuPoId(int id) {

      return repository.findById(id).orElse(null);
   }

   public Narudžba spremiNarudžbu(Narudžba narudžba, String userName) {

      Narudžba novaNarudžba = new Narudžba();
      novaNarudžba.setId_Korisnika(izvuciKorisnikId(userName));
      novaNarudžba.setNaruceniArtikli(narudžba.getNaruceniArtikli());
      novaNarudžba.setVrstaPlacanja(narudžba.getVrstaPlacanja());
      novaNarudžba.setVrijeme(narudžba.getVrijeme());
      novaNarudžba.setUkupanPorez(narudžba.getUkupanPorez());
      novaNarudžba.setUkupanIznos(narudžba.getUkupanIznos());
      repository.save(novaNarudžba);
      return novaNarudžba;
   }

   private int izvuciKorisnikId(String username) {

      User user = userRepository.findByUserName(username).orElse(null);
      if (user != null) {
         return user.getId();
      }
      return 0;
   }
}
