package aplikacija.za.upravljanje.restoranom.Service;

import aplikacija.za.upravljanje.restoranom.Entiteti.Narudžba;
import aplikacija.za.upravljanje.restoranom.Entiteti.NarudzbaDto;
import aplikacija.za.upravljanje.restoranom.Entiteti.User;
import aplikacija.za.upravljanje.restoranom.Repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NaručeniArtikliService {

   public static Logger logger = LoggerFactory.getLogger(NaručeniArtikliService.class);

   private NarudžbaService service;
   private UserRepository userRepositroy;

   public NaručeniArtikliService(NarudžbaService service,UserRepository userRepositroy) {

      this.service = service;
      this.userRepositroy = userRepositroy;
   }


   public void kreirajNarudzbu(String username, List<NarudzbaDto> naruceniArtikli) {

      //Za ve je hardcodano vrsta placanja
      Narudžba narudžba = new Narudžba();
      double ukupanIznosRacuna = getUkupanIznosRacuna(naruceniArtikli);
      double ukupanPorez = getUkupanPorez(ukupanIznosRacuna);
      int idKorisnika = izvadiIdKorisnika(username);
      LocalTime vrijeme = LocalTime.now();
      List<List<Object>> artikli = izvuciArtikle(naruceniArtikli);
      //Zapis v bazu sih artikli v listi
      String sviArtikli = artikli.stream()
                             .map(n -> String.valueOf(n))
                             .collect(Collectors.joining(",", "{", "}"));

      System.out.println(sviArtikli);

      //Dodati provjeru da ako je null kaj se na frontu ispise poruka : odaberite neke iteme barem ... tak nes otp
      narudžba.setUkupanIznos(ukupanIznosRacuna);
      narudžba.setNaruceniArtikli(sviArtikli);
      narudžba.setUkupanPorez(ukupanPorez);
      narudžba.setVrijeme(vrijeme);
      narudžba.setId_Korisnika(idKorisnika);
      narudžba.setVrstaPlacanja("Kartica");
      service.spremiNarudžbu(narudžba,username);

   }

   private List<List<Object>> izvuciArtikle(List<NarudzbaDto> naruceniArtikli) {

      List<List<Object>> sadržajNarudzbe = new ArrayList<>();
      for(NarudzbaDto narudzba : naruceniArtikli){
         List<Object> artikli = new ArrayList<>();
         int id = narudzba.getId();
         String kolicina = String.valueOf(narudzba.getKolicina());
         String cijena = String.valueOf(narudzba.getCijena());
         artikli.add(id);
         artikli.add(kolicina);
         artikli.add(cijena);
         sadržajNarudzbe.add(artikli);
      }

      return sadržajNarudzbe;
   }

   private double getUkupanPorez(double ukupanIznosRacuna) {
      double ukupanPorez = 0;
      if(ukupanIznosRacuna > 0){
         ukupanPorez = ukupanIznosRacuna * ((double)13/(double)100);
      }
      return ukupanPorez;
   }

   private double getUkupanIznosRacuna(List<NarudzbaDto> naruceniArtikli) {

      double ukupnaIznosRacuna = 0;
         for (NarudzbaDto artikal : naruceniArtikli) {
            double cijenaArtikla = artikal.getCijena();
            int kolicina = artikal.getKolicina();
            double iznos = cijenaArtikla * kolicina;
            ukupnaIznosRacuna += iznos;
         }

      return ukupnaIznosRacuna;
   }

   private int izvadiIdKorisnika(String username) {

      User user = userRepositroy.findByUserName(username).orElse(null);
      if (user != null) {
         return user.getId();
      }
      return 0;
   }
}
