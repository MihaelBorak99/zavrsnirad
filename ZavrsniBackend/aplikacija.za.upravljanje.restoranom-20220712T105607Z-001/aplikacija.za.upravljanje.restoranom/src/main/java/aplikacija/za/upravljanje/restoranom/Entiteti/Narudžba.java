package aplikacija.za.upravljanje.restoranom.Entiteti;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalTime;


@Table(name = "narudzba")
@Entity
public class Narudžba {

   @Id
   @GeneratedValue
   private int id;
   private int id_Korisnika;
   private LocalTime vrijeme;
   private String naruceniArtikli;
   private String vrstaPlacanja;
   private double ukupanIznos;
   private double ukupanPorez;

   public Narudžba() {

   }

   public Narudžba(int id,
                   int id_Korisnika,
                   LocalTime vrijeme,
                   String naruceniArtikli,
                   String vrstaPlacanja,
                   double ukupanIznos,
                   double ukupanPorez) {

      this.id = id;
      this.id_Korisnika = id_Korisnika;
      this.vrijeme = vrijeme;
      this.naruceniArtikli = naruceniArtikli;
      this.vrstaPlacanja = vrstaPlacanja;
      this.ukupanIznos = ukupanIznos;
      this.ukupanPorez = ukupanPorez;
   }

   public int getId() {

      return id;
   }

   public void setId(int id) {

      this.id = id;
   }

   public int getId_Korisnika() {

      return id_Korisnika;
   }

   public void setId_Korisnika(int id_Korisnika) {

      this.id_Korisnika = id_Korisnika;
   }

   public LocalTime getVrijeme() {

      return vrijeme;
   }

   public double getUkupanIznos() {

      return ukupanIznos;
   }

   public void setUkupanIznos(double ukupanIznos) {

      this.ukupanIznos = ukupanIznos;
   }

   public double getUkupanPorez() {

      return ukupanPorez;
   }

   public void setUkupanPorez(double ukupanPorez) {

      this.ukupanPorez = ukupanPorez;
   }

   public void setVrijeme(LocalTime vrijeme) {

      this.vrijeme = vrijeme;
   }

   public String getNaruceniArtikli() {

      return naruceniArtikli;
   }

   public void setNaruceniArtikli(String naruceniArtikli) {

      this.naruceniArtikli = naruceniArtikli;
   }

   public String getVrstaPlacanja() {

      return vrstaPlacanja;
   }

   public void setVrstaPlacanja(String vrstaPlacanja) {

      this.vrstaPlacanja = vrstaPlacanja;
   }
}
