package aplikacija.za.upravljanje.restoranom.Service;

import aplikacija.za.upravljanje.restoranom.Entiteti.Artikal;
import aplikacija.za.upravljanje.restoranom.Repository.Artikal_Repository;
import aplikacija.za.upravljanje.restoranom.Entiteti.User;
import aplikacija.za.upravljanje.restoranom.Repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArtikalService {

   private final Artikal_Repository repository;
   private final UserRepository userRepository;

   public ArtikalService(Artikal_Repository repository, UserRepository userRepository) {

      this.repository = repository;
      this.userRepository = userRepository;
   }

   public Artikal dohvatiArtikalPoImenu(String naziv) {

      return repository.findByNaziv(naziv);
   }

   public Artikal dohvatiArtikalPoId(int id) {

      return repository.findById(id).orElse(null);
   }

   public List<Artikal> dohvatiSveArtikle(String username) {

      if (isAdmin(username)) {
         return repository.findAll();
      }
      return Collections.emptyList();
   }

   //Filtrira sve nema veze ako je upper/Lower case
   public List<Artikal> filtrirajSveArtikle(String filtar) {

      List<Artikal> preFilter = repository.findAll();
      return preFilter.stream()
                      .filter(artikal -> artikal.getNaziv().toLowerCase()
                                                .contains(filtar.toLowerCase()))
                      .collect(Collectors.toList());
   }

   public Artikal spremiArtikal(Artikal artikal) {

      //Dodaj novi - ukoliko ne postoji frontend salje id-1 ako postoji , updejta postojeceg tak da obrises stari i dodas novi zapis
      if (artikal.getId() == -1 || artikal.getId() == 0) {

         Artikal noviArtikal = new Artikal();
         noviArtikal.setCijena(artikal.getCijena());
         noviArtikal.setKategorija(artikal.getKategorija());
         noviArtikal.setNaziv(artikal.getNaziv());
         repository.save(noviArtikal);
         return noviArtikal;
      }
      else {
         repository.deleteById(artikal.getId());
         repository.save(artikal);
         return artikal;
      }
   }

   public void obrisiArtikalPoId(int id) {

      repository.deleteById(id);
   }

   private boolean isAdmin(String username) {

      User user = userRepository.findByUserName(username).orElse(null);
      if (user != null && user.getRoles().equals("ADMIN")) {
         return true;
      }
      return false;
   }
}
