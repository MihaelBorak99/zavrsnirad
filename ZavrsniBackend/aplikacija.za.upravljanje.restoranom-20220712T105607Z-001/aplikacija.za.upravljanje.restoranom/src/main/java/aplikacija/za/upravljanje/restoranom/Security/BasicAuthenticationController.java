package aplikacija.za.upravljanje.restoranom.Security;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class BasicAuthenticationController {

   @GetMapping(path = "/basicAuth")
   public AuthenticationBean helloWorldBean(){
      return new AuthenticationBean("You are authenticated");
   }
   //Moze se također vratiti role usera kaj se zna dalje kaj s njim.... tipa dal je admin ili normal user
}
