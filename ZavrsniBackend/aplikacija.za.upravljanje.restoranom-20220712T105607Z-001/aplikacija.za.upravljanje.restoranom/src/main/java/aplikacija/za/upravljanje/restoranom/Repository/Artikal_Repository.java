package aplikacija.za.upravljanje.restoranom.Repository;

import aplikacija.za.upravljanje.restoranom.Entiteti.Artikal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Artikal_Repository extends JpaRepository<Artikal, Integer> {

   Artikal findByNaziv(String naziv);
}
