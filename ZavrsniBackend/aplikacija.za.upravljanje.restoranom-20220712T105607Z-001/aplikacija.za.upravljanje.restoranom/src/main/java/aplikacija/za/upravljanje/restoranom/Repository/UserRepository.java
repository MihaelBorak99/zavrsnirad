package aplikacija.za.upravljanje.restoranom.Repository;

import aplikacija.za.upravljanje.restoranom.Entiteti.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Integer> {

   Optional<User> findByUserName(String userName);

}
