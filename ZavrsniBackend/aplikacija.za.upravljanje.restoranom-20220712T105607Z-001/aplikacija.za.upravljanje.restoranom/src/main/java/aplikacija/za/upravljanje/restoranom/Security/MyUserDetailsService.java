package aplikacija.za.upravljanje.restoranom.Security;

import aplikacija.za.upravljanje.restoranom.Repository.UserRepository;
import aplikacija.za.upravljanje.restoranom.Entiteti.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MyUserDetailsService implements UserDetailsService {

   final
   UserRepository userRepository;

   public MyUserDetailsService(UserRepository userRepository) {

      this.userRepository = userRepository;
   }

   @Override
   public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
      Optional<User> user = userRepository.findByUserName(userName);

      user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));

      return user.map(MyUserDetails::new).get();
   }

}
